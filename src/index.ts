import Board from "./components/Board";
import World from "./components/World";
import chalk from 'chalk';
import figlet from 'figlet';
import { createInterface, ReadLineOptions } from 'readline'
import { ConsoleMessages, showOutput } from "./constants/console-messages";

const board = new Board()
const world = new World(board)

const readLineOptions: ReadLineOptions = {
  input: process.stdin,
  output: process.stdout
}

const readline = createInterface(readLineOptions)

const init = (): void => {
  console.log(
    figlet.textSync(ConsoleMessages.TITLE, {
      font: "Banner3-D",
    })
  )
  showOutput(chalk.redBright(ConsoleMessages.ROBOT_EMOJI) + " " + ConsoleMessages.DESC)
  showOutput(ConsoleMessages.INTROC)
}

init()
readline.prompt(true)
readline.on("line", (line: string) => {
  world.execute(line)
  readline.prompt(true)
})