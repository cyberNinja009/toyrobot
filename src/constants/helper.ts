import Board from "../components/Board"
import { IState, Directions } from "../components/Robot"
import { showError, ConsoleMessages, showPlacementError } from "./console-messages"

export const isValidPlacement = (placeOption: IState, board: Board): boolean => {
  const isValidPosition = board.isPositionValid(placeOption.position)
  const isValidDirection = Object.keys(Directions).includes(placeOption.direction)
  if (!isValidPosition) showError(ConsoleMessages.INVALID_POSITION)
  if (!isValidDirection) showError(ConsoleMessages.INVALID_DIRECTION)
  return isValidPosition && isValidDirection
}

export const placementOptionsHandler = (optionalCommandStr: string): IState | undefined => {
  try {
    const coordinates = optionalCommandStr.split(",")
    const x = Number(coordinates[0])
    const y = Number(coordinates[1])
    const direction = coordinates[2] as Directions
    const placementOptions: IState = { position: { x: x, y: y }, direction: direction }
    return placementOptions
  } catch {
    showPlacementError()
    return undefined;
  }
}

interface ICommand {
  action: string
  placementOptions: string
}

export const commandParser = (commandLine: string): ICommand | undefined => {
  try {
    const rawCommandArr = commandLine.trim().toUpperCase().split(" ");
    const action = rawCommandArr[0]
    const optionalCommandStr = rawCommandArr[1]
    return { action: action, placementOptions: optionalCommandStr }
  } catch {
    showError(ConsoleMessages.INVALID_COMMAND)
  }
}