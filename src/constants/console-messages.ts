import chalk from "chalk"

export enum ConsoleMessages {
    TITLE = "Toy Robot",
    DESC = "Toy Rboot Cli for REA coding challenge",
    INTROC = "Please enter your command:",
    INVALID_POSITION = "Invalid position value, please try again.",
    INVALID_DIRECTION = "Invalid facing direction value, please try again",
    INVALID_PLACEMENT = "Invalid parameters for 'PLACE'",
    INVALID_COMMAND = "Invalid command, please try again",
    HELP = `
    Commands List:

    PLACE $x,$y,$DIRECTION 	---> Places the robot using xy coordinates(0≤x≤4, 0≤y≤4) with a proper facing direction( available NORTH | SOUTH | EAST | WEST ) to the Board
    MOVE 			---> Move the toy robot one unit forward in the direction it is currently facing
    RIGHT 			---> Rotate the robot 90 degrees in clockwise direction
    LEFT 			---> Rotate the robot 90 degrees in anticlockwise direction
    REPORT 			---> Anounce the robot's position and facing direction on the table    
    HELP 			---> Get command list

    *If the robot is not placed, it will ignore the MOVE, LEFT, RIGHT and REPORT commands
    `,
    ROBOT_EMOJI = `d[o_0]b`
}

export const showOutput = (info: string): void => {
  console.info(info)
}

export const showError = (info: string): void => {
  console.error(chalk.red(info))
}

export const showPlacementError = () : void => {
  showError(ConsoleMessages.INVALID_PLACEMENT)
  showOutput(chalk.cyan(ConsoleMessages.HELP))
}