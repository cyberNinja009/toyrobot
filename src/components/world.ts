import chalk from "chalk"
import Board from "./Board"
import Robot from "./Robot"
import { isValidPlacement, placementOptionsHandler } from "../constants/helper"
import { ConsoleMessages, showError, showOutput } from "../constants/console-messages"

export default class World {
  private robot?: Robot
  private board: Board

  constructor (board: Board) {
    this.board = board
  }

  execute (commandLine: string): void {
    const raw = commandLine.trim().toUpperCase().split(" ");
    const action = raw[0]
    switch (action) {
    case "PLACE": {
      const placeOption = placementOptionsHandler(raw[1])
      if (placeOption && isValidPlacement(placeOption, this.board)) {
        if (!this.robot) {
          this.robot = new Robot(placeOption.position, placeOption.direction)
          this.robot.isPlaced = true
        } else {
          this.robot.place(placeOption)
        }
      }
      break
    }
    case "MOVE":
      this.robot?.move(this.board.maxX, this.board.maxY)
      break
    case "LEFT":
      this.robot?.left()
      break
    case "RIGHT":
      this.robot?.right()
      break
    case "REPORT":
      this.robot?.isPlaced && showOutput(chalk.green(this.robot?.report()))
      break
    case "HELP":
      showOutput(ConsoleMessages.HELP)
      break
    default:
      showError(ConsoleMessages.INVALID_COMMAND)
    }
  }
}