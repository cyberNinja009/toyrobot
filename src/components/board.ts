export interface IPosition {
    x: number
    y: number
}

export default class Board {

    public length: number
    public width: number
    public maxX: number
    public maxY: number
    constructor (length = 5, width = 5) {
      this.length = length
      this.width = width
      this.maxX = length - 1
      this.maxY = width - 1
    }

    isPositionValid (position: IPosition): boolean {
      return this.isGreaterThanMinSize(position) && this.isLessThanMaxSize(position)
    }

    private isGreaterThanMinSize (position: IPosition): boolean {
      return position.x >= 0 && position.y >= 0
    }

    private isLessThanMaxSize (position: IPosition): boolean {
      return position.x <= this.maxX && position.y <= this.maxY
    }
}