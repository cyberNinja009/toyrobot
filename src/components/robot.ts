import { IPosition } from "./Board";

export enum Directions {
  NORTH = "NORTH",
  EAST = "EAST",
  SOUTH = "SOUTH",
  WEST = "WEST"
}

export interface IState {
  position: IPosition
  direction: Directions
}

interface ICompassElement {
  [x: string]: {
    left: Directions
    right: Directions
  }
}

const compass: ICompassElement = {
  NORTH: {
    left: Directions.WEST,
    right: Directions.EAST
  },
  EAST: {
    left: Directions.NORTH,
    right: Directions.SOUTH
  },
  SOUTH: {
    left: Directions.EAST,
    right: Directions.WEST
  },
  WEST: {
    left: Directions.SOUTH,
    right: Directions.NORTH
  }
}

export default class Robot {
  currentPosition: IPosition | undefined
  currentDirection: Directions | undefined
  isPlaced: boolean | undefined

  constructor (position?: IPosition, direction?: Directions) {
    this.currentPosition = position
    this.currentDirection = direction
  }

  place = (state: IState): boolean => {
    this.currentPosition = state.position
    this.currentDirection = state.direction
    if (!this.isPlaced) this.isPlaced = true
    return true
  }

  move = (maxX: number, maxY: number): boolean => {
    if (!this.isPlaced) {
      return false
    }
    if (this.currentDirection && this.currentPosition) {
      switch (this.currentDirection) {
      case Directions.NORTH:
        if (this.currentPosition.y < maxY) this.currentPosition.y++
        break;
      case Directions.EAST:
        if (this.currentPosition.x < maxX) this.currentPosition.x++
        break
      case Directions.SOUTH:
        if (this.currentPosition.y > 0) this.currentPosition.y--
        break
      case Directions.WEST:
        if (this.currentPosition.x > 0) this.currentPosition.x--;
      }
      return true
    }
    return false
  }

  left = (): boolean => {
    if (!this.isPlaced) {
      return false
    }
    const resultDirection = this.currentDirection && compass[this.currentDirection].left
    if (resultDirection) {
      this.currentDirection = resultDirection;
      return true
    }
    return false
  }

  right = (): boolean => {
    if (!this.isPlaced) {
      return false
    }
    const resultDirection = this.currentDirection && compass[this.currentDirection].right
    if (resultDirection) {
      this.currentDirection = resultDirection;
      return true
    }
    return false
  }

  report = (): string => `${this.currentPosition?.x},${this.currentPosition?.y},${this.currentDirection}`
}