# Toy Robot CLI

## Description

Toy Robot is a very popular coding puzzle, and it’s been around since 2007. 
Recently I challenged my skills with it, and overhere is my solution with typescript.

## Installation

Prerequisites: Node.js (>=12.18.4)

In root of the app run yarn install

> \$ yarn install

## Usage

Run the program from the app root

> \$ yarn start

## Scripts List

- `build` - transpile TypeScript,
- `clean` - Remove transpiled files,
- `clean:all` - Remove transpiled files as well as ./node_modules ./package-lock.json ./yarn.lock
- `test` - Run unit tests and display individual test results with the test suite hierarchy.,
- `test:coverage` - Test coverage information will be reported in the output
- `test:watch` - Interactive watch mode to automatically re-run tests,
- `lint` - Linting with ESlint

## Test

> \$ yarn test

It will display individual test results with the test suite hierarchy

> \$ yarn test:coverage

It will generate a test coverage report

## License

It is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).