import Board from "../../src/components/Board"
import { Directions } from "../../src/components/Robot"
import { ConsoleMessages } from "../../src/constants/console-messages"
import { commandParser, isValidPlacement, placementOptionsHandler } from "../../src/constants/helper"

const consoleErrorSpy = jest.spyOn(console, "error").mockImplementation()
const consoleInfoSpy = jest.spyOn(console, 'info').mockImplementation()

describe("Helper", () => {
  describe("IsValidPlacement", () => {
    beforeEach(() => {
      consoleErrorSpy.mockClear()
      consoleInfoSpy.mockClear()
    })
    const board = new Board();
    test("Would be true if valid position and direction are given", () => {
      const placement = {
        position: {
          x: 3,
          y: 2
        },
        direction: Directions.WEST
      }
      const validity = isValidPlacement(placement, board)
      expect(validity).toBeTruthy()
    })

    test("Would be false if invalid position is given", () => {
      const placement = {
        position: {
          x: 3,
          y: 6
        },
        direction: Directions.WEST
      }
      const validity = isValidPlacement(placement, board)
      expect(validity).toBeFalsy()
      expect(console.error).toHaveBeenCalled();
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_POSITION
      );
    })

    test("Would be false if invalid direction is given", () => {
      const placement = {
        position: {
          x: 3,
          y: 2
        },
        direction: "FARWEST" as Directions
      }
      const validity = isValidPlacement(placement, board)
      expect(validity).toBeFalsy()
      expect(console.error).toHaveBeenCalled();
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_DIRECTION
      );
    })

    test("Would be false if invalid position and invalid direction are given", () => {
      const placement = {
        position: {
          x: 8,
          y: 2
        },
        direction: "FARWEST" as Directions
      }
      const validity = isValidPlacement(placement, board)
      expect(validity).toBeFalsy()
      expect(console.error).toHaveBeenCalled();
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_POSITION
      )
      expect(consoleErrorSpy.mock.calls[1][0]).toContain(
        ConsoleMessages.INVALID_DIRECTION
      )
    })
  })

  describe("Placement Options handler", () => {
    beforeEach(() => {
      consoleErrorSpy.mockClear()
    })
    test("Should return coverted options", () => {
      const options = placementOptionsHandler("3,4,SOUTH")
      expect(options).toEqual({
        position: { x: 3, y: 4 },
        direction: Directions.SOUTH
      })
    })

    test("Should return undefined", () => {
      const options = placementOptionsHandler( undefined as any)
      expect(options).toEqual(undefined)
      expect(console.error).toHaveBeenCalled()
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_PLACEMENT
      )
      expect(console.info).toHaveBeenCalled()
    })
  })

  describe("Command Parser", () => {
    beforeEach(() => {
      consoleErrorSpy.mockClear()
      consoleInfoSpy.mockClear()
    })
    
    test("Should return 'PLACE' and the placement options", () => {
      const res = commandParser("PLACE 3,4,NORTH")
      expect(res).toEqual({action: "PLACE", placementOptions: "3,4,NORTH"})
    })

    test("Should return the whole string if it is not complied with the rule of `PLACE` command", () => {
      const res = commandParser("PLACE3,4,NORTH")
      expect(res).toEqual({action: "PLACE3,4,NORTH", placementOptions: undefined})
    })

    test("Should return 'MOVE'", () => {
      const res = commandParser("MOVE")
      expect(res).toEqual({action: "MOVE", placementOptions: undefined})
    })

    test("Should return 'LEFT'", () => {
      const res = commandParser("LEFT")
      expect(res).toEqual({action: "LEFT", placementOptions: undefined})
    })

    test("Should return 'RIGHT'", () => {
      const res = commandParser("RIGHT")
      expect(res).toEqual({action: "RIGHT", placementOptions: undefined})
    })

    test("Should return 'REPORT'", () => {
      const res = commandParser("REPORT")
      expect(res).toEqual({action: "REPORT", placementOptions: undefined})
    })

    test("Should return 'HELP'", () => {
      const res = commandParser("HELP")
      expect(res).toEqual({action: "HELP", placementOptions: undefined})
    })

    test("Should return undefined and print error of invalid command", () => {
      const res = commandParser(undefined as any)
      expect(res).toEqual(undefined)
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_COMMAND
      )
      expect(console.error).toHaveBeenCalledTimes(1)
    })
  })
})