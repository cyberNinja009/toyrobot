import { mocked } from "ts-jest/utils";
import Board from "../../src/components/Board";
import Robot, { Directions } from "../../src/components/Robot";
import World from "../../src/components/World";
import { ConsoleMessages } from "../../src/constants/console-messages";

jest.mock("../../src/components/Robot")

const mockedToybot = mocked(Robot, true);
mockedToybot.prototype.move = jest.fn()
mockedToybot.prototype.left = jest.fn()
mockedToybot.prototype.right = jest.fn()
mockedToybot.prototype.report = jest.fn()

const consoleErrorSpy = jest.spyOn(console, 'error').mockImplementation()
const consoleInfoSpy = jest.spyOn(console, 'info').mockImplementation()

describe("Command", () => {
  const board = new Board()
  const world = new World(board)
  describe("Toybot is not called by the Board class constructor", () => {
    expect(mockedToybot).not.toHaveBeenCalled();
  });
  describe("Execute `PLACE` command", () => {
    world.execute("PLACE 3,3,SOUTH")
    beforeEach(() => {
      consoleErrorSpy.mockClear()
      consoleInfoSpy.mockClear()
    })

    describe("'PLACE' command", () => {
      test("Command 'PLACE' will create a Robot instance", () => {
        expect(mockedToybot).toHaveBeenCalled()
        expect(mockedToybot.mock.calls[0][0]).toEqual(
          { x: 3, y: 3 }
        )
        expect(mockedToybot.mock.calls[0][1]).toEqual(
          Directions.SOUTH
        )
      })

      test("Should print error when robot's position is invalid", () => {
        world.execute("PLACE 8,-9,NORTH")
        expect(consoleErrorSpy.mock.calls[0][0]).toContain(
          ConsoleMessages.INVALID_POSITION
        )
        expect(console.error).toHaveBeenCalledTimes(1)
      })

      test("Should print error when robot's direction is invalid", () => {
        world.execute("PLACE 1,2,UNKNOWN")
        expect(consoleErrorSpy.mock.calls[0][0]).toContain(
          ConsoleMessages.INVALID_DIRECTION
        )
        expect(console.error).toHaveBeenCalledTimes(1)
      })

      test("Should print errors when robot's position and direction are invalid", () => {
        world.execute("PLACE -6,2,UNKNOWN")
        expect(consoleErrorSpy.mock.calls[0][0]).toContain(
          ConsoleMessages.INVALID_POSITION
        )
        expect(consoleErrorSpy.mock.calls[1][0]).toContain(
          ConsoleMessages.INVALID_DIRECTION
        )
        expect(console.error).toHaveBeenCalledTimes(2)
      })

      test("Should only robot one time if robot is not null", () => {
        world["robot"] = mockedToybot.mock.instances[0]
        world["robot"]["place"] = jest.fn()
        world.execute("PLACE 2,2,NORTH")
        expect(mockedToybot).toHaveBeenCalledTimes(1)
      })

    })
  })

  describe("Excecute 'MOVE', 'LEFT', 'RIGHT'and 'REPORT' commands", () => {
    test("Command 'MOVE' will move the robot 1 unit", () => {
      world.execute("MOVE")
      expect(mockedToybot.mock.instances[0].move).toBeCalled()
    })

    test("Command 'LEFT' will turn the robot to the left", () => {
      world.execute("LEFT")
      expect(mockedToybot.mock.instances[0].left).toBeCalled()
    })

    test("Command 'RIGHT' will turn the robot to the right", () => {
      world.execute("RIGHT")
      expect(mockedToybot.mock.instances[0].right).toBeCalled()
    })

    test("Command 'REPORT' will report the current status of the robot", () => {
      world.execute("REPORT")
      expect(mockedToybot.mock.instances[0].report).toBeCalled()
      expect(console.info).toHaveBeenCalledTimes(1)
    })

    test("Should print error when invalid command is given", () => {
      world.execute("UNKOWN")
      expect(consoleErrorSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.INVALID_COMMAND
      )
      expect(console.error).toHaveBeenCalledTimes(1)
    })
  })

  describe("'HELP' command", () => {
    beforeEach(() => {
      consoleInfoSpy.mockClear()
    })
    test("Shall print out the command list", () => {
      world.execute("HELP")
      expect(console.info).toHaveBeenCalledTimes(1)
      expect(consoleInfoSpy.mock.calls[0][0]).toContain(
        ConsoleMessages.HELP
      )
    })
  })
})