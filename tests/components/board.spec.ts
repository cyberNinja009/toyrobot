import Board from '../../src/components/Board';

describe("Create a Board", () => {
  describe("Create a new board with no arguments given", () => {
    const board = new Board()
    test("Should instantiate a new 5 * 5 board", () => {
      expect(board).not.toBeNull()
      expect(board.length).toBe(5)
      expect(board.width).toBe(5)
      expect(board.maxX).toBe(4)
      expect(board.maxY).toBe(4)
    });
  })

  describe('Create a new board with argument', () => {
    const board = new Board(8, 8)
    it('Should instantiate a new 9 * 9 board', () => {
      expect(board).not.toBeNull()
      expect(board.length).toBe(8)
      expect(board.width).toBe(8)
      expect(board.maxX).toBe(7)
      expect(board.maxY).toBe(7)
    });
  });

  describe("Is a valid position in a 5 * 5 board", () => {
    const board = new Board()
    test.each`
            x    | y    | expected
            ${0} |${0}  | ${true}
            ${0} |${4}  | ${true}
            ${4} |${4}  | ${true}
            ${0} |${4}  | ${true}
            ${3} |${1}  | ${true}
            ${2} |${-1} | ${false}
            ${-3}|${2}  | ${false}
        `(`should return $expected if x=$x y=$y`, ({ x, y, expected }) => {
      const result = board.isPositionValid({ x, y })
      expect(result).toBe(expected)
    })
  })
});