import Board, { IPosition } from "../../src/components/Board";
import Robot, { Directions } from "../../src/components/Robot";

describe("Activate the robot", () => {
  test("A new toy robot should be initialised", () => {
    expect(new Robot({x:2, y:2}, Directions.EAST)).not.toBeNull();
  });
});

describe("Placing a toy robot on the board", () => {
  let bot: Robot;

  beforeEach(() => {
    bot = new Robot({x:2, y:2}, Directions.NORTH);
  })

  test("Can place a new toy robot on the board", () => {
    const coordinates = { position: { x: 0, y: 0 }, direction: Directions.NORTH }
    bot.place(coordinates);
    expect(bot.report()).toEqual("0,0,NORTH");
  });
});

describe("Moving a toy robot", () => {
  let bot: Robot;
  let board: Board

  beforeEach(() => {
    bot = new Robot({x:2, y:2}, Directions.EAST);
    board = new Board()
  })

  test("Cannot move a bot that isn't placed", () => {
    expect(bot.move(board.maxX, board.maxY)).toBeFalsy();
  });

  test("Should return false if position and direction are not given", () => {
    bot.isPlaced = true
    bot.currentPosition = undefined
    bot.currentDirection = undefined
    const res = bot.move(board.maxX, board.maxY)
    expect(res).toBeFalsy()
  })

  test("Can move a bot that is on the board facing NORTH", () => {
    bot.place({
      position: {
        x: 0,
        y: 1
      },
      direction: Directions.NORTH
    });
    expect(bot.move(board.maxX, board.maxY)).toBeTruthy();
    expect(bot.report()).toEqual("0,2,NORTH");
  });

  test("Can move a bot that is on the board facing EAST", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.EAST });
    expect(bot.move(board.maxX, board.maxY)).toBeTruthy();
    expect(bot.report()).toEqual("1,0,EAST");
  });

  test("Can move a bot that is on the board facing SOUTH", () => {
    bot.place({ position: { x: 0, y: 1 }, direction: Directions.SOUTH });
    expect(bot.move(board.maxX, board.maxY)).toBeTruthy();
    expect(bot.report()).toEqual("0,0,SOUTH");
  });

  test("Can move a bot that is on the board facing WEST", () => {
    bot.place({ position: { x: 1, y: 0 }, direction: Directions.WEST });
    expect(bot.move(board.maxX, board.maxY)).toBeTruthy();
    expect(bot.report()).toEqual("0,0,WEST");
  });

  test("Must not fall off the table during movement", () => {
    bot.place({position: {x: 4, y:0}, direction: Directions.EAST})
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("4,0,EAST")
    bot.right()
    expect(bot.report()).toEqual("4,0,SOUTH")
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("4,0,SOUTH")

    bot.place({position: {x: 4, y:4}, direction: Directions.NORTH})
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("4,4,NORTH")
    bot.right()
    expect(bot.report()).toEqual("4,4,EAST")
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("4,4,EAST")

    bot.place({position: {x: 0, y:0}, direction: Directions.SOUTH})
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("0,0,SOUTH")
    bot.right()
    expect(bot.report()).toEqual("0,0,WEST")
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("0,0,WEST")

    bot.place({position: {x: 0, y:4}, direction: Directions.NORTH})
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("0,4,NORTH")
    bot.left()
    expect(bot.report()).toEqual("0,4,WEST")
    bot.move(board.maxX, board.maxY)
    expect(bot.report()).toEqual("0,4,WEST")
  })
});

describe("A bot without current position/ is not placed", () => {
  const robot = new Robot()
  const maxX = 5000;
  const maxY = 5000;
  test("Moving should return false", () => {
    const res = robot.move(maxX, maxY)
    expect(res).toBeFalsy()
  })

  test("Turning left should return false", () => {
    const res = robot.left()
    expect(res).toBeFalsy()
  })

  test("Turning right should return false", () => {
    const res = robot.right()
    expect(res).toBeFalsy()
  })

  test("Report should return 'undefined,undefined,undefined'", () => {
    expect(robot.report()).toEqual("undefined,undefined,undefined")
  })
})

describe("TurnLeft", () => {
  let bot: Robot;

  beforeEach(() => {
    bot = new Robot({x:2, y:2}, Directions.NORTH);
  })

  test("Should return false if position and direction are not given", () => {
    bot.isPlaced = true
    bot.currentPosition = undefined
    bot.currentDirection = undefined
    const res = bot.left()
    expect(res).toBeFalsy()
  })

  test("Turning left changes from NORTH to WEST", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.NORTH });
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,WEST");
  });

  test("Turning left changes from EAST to NORTH", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.EAST });
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,NORTH");
  });

  test("Turning left changes from SOUTH to EAST", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.SOUTH });
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,EAST");
  });

  test("Turning right changes from WEST to NORTH", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.WEST });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,NORTH");
  });

  test("Turning left * 4 times, should return to origianl facing direction", () => {
    const position: IPosition = { x: 3, y: 3 }
    bot.place({ position, direction: Directions.NORTH });
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,WEST");
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,SOUTH");
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,EAST");
    expect(bot.left()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,NORTH");
  });
});

describe("Turning right", () => {
  let bot: Robot;
  const position: IPosition = { x: 0, y: 0 }
  beforeEach(() => {
    bot = new Robot();
  })

  test("Cannot change direction of a bot that isn't placed", () => {
    expect(bot.right()).toBeFalsy();
  });

  test("Should return false if position and direction are not given", () => {
    bot.isPlaced = true
    bot.currentPosition = undefined
    bot.currentDirection = undefined
    const res = bot.right()
    expect(res).toBeFalsy()
  })

  test("Turning right changes from NORTH to EAST", () => {

    bot.place({ position, direction: Directions.NORTH });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,EAST");
  });

  test("Turning right changes from EAST to SOUTH", () => {
    bot.place({ position, direction: Directions.EAST });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,SOUTH");
  });

  test("Turning right changes from SOUTH to WEST", () => {
    bot.place({ position, direction: Directions.SOUTH });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,WEST");
  });

  test("Turning right changes from WEST to NORTH", () => {
    bot.place({ position, direction: Directions.WEST });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("0,0,NORTH");
  });

  test("Turning right * 4 times", () => {
    const pos2: IPosition = { x: 3, y: 3 }
    bot.place({ position: pos2, direction: Directions.NORTH });
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,EAST");
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,SOUTH");
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,WEST");
    expect(bot.right()).toBeTruthy();
    expect(bot.report()).toEqual("3,3,NORTH");
  });

});

describe("Reporting location of toy robot", () => {
  let bot: Robot;

  beforeEach(() => {
    bot = new Robot();
  })

  test("Can report location of a bot that is on the board", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.NORTH });
    expect(bot.report()).toEqual("0,0,NORTH");
  });

  test("Can report different location of a bot that is on the board", () => {
    const location = {
      position: {
        x: 1,
        y: 4
      },
      direction: Directions.WEST
    }
    bot.place(location);
    expect(bot.report()).toEqual("1,4,WEST");
  });
});

describe("Running through the example input with a proper board", () => {
  let bot: Robot;
  let board: Board

  beforeEach(() => {
    bot = new Robot();
    board = new Board();
  })

  test("PLACE (0,0,NORTH) -> MOVE -> REPORT", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.NORTH });
    bot.move(board.maxX, board.maxY);
    expect(bot.report()).toEqual("0,1,NORTH");
  });

  test("PLACE (0,0,NORTH) -> LEFT -> REPORT", () => {
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.NORTH });
    bot.left();
    expect(bot.report()).toEqual("0,0,WEST");
  });

  test("PLACE (1,2,EAST) -> MOVE -> MOVE -> LEFT -> MOVE -> REPORT", () => {
    bot.place({ position: { x: 1, y: 2 }, direction: Directions.EAST });
    bot.move(board.maxX, board.maxY);
    bot.move(board.maxX, board.maxY);
    bot.left();
    bot.move(board.maxX, board.maxY);
    expect(bot.report()).toEqual("3,3,NORTH");
  });
})

describe("Running through mocked boarddata", () => {
  let bot: Robot;
  
  beforeEach(() => {
    bot = new Robot();
  });
  
  test("PLACE (0,0,NORTH) -> MOVE -> REPORT", () => {
    const maxX = 5000000;
    const maxY = 5000000;
    bot.place({ position: { x: 0, y: 0 }, direction: Directions.NORTH });
    bot.move(maxX, maxY);
    expect(bot.report()).toEqual("0,1,NORTH");
  });
  
  test("PLACE (1,2,EAST) -> MOVE -> MOVE -> LEFT -> MOVE -> REPORT", () => {
    const maxX = 8000000;
    const maxY = 8000000;
    bot.place({ position: { x: 1, y: 2 }, direction: Directions.EAST });
    bot.move(maxX, maxY);
    bot.move(maxX, maxY);
    bot.left();
    bot.move(maxX, maxY);
    expect(bot.report()).toEqual("3,3,NORTH");
  });
})